import { PUBLIC_KEY, HASH_KEY } from '../../src/utils/constants'

import GlobalStyles from '../../styles/globals'
import Caracter from '../../src/components/templates/Caracter'

const CaracterInfo = ({ data }) => (
  <>
    <GlobalStyles />
    <Caracter data={data} />
  </>
)

export const getStaticProps = async ({ params }) => {
  const id = parseInt(params?.id)
  const url = `http://gateway.marvel.com/v1/public/characters/${id}?ts=1&apikey=${PUBLIC_KEY}&hash=${HASH_KEY}`

  const response = await fetch(url)

  const data = await response.json()

  return {
    props: {
      data
    }
  }
}

export async function getStaticPaths() {
  const url = `http://gateway.marvel.com/v1/public/characters?ts=1&offset=0&limit=100&apikey=${PUBLIC_KEY}&hash=${HASH_KEY}`
  const response = await fetch(url)

  const data = await response.json()

  const paths = data?.data?.results?.map((item) => ({
    params: { id: `${item.id}` }
  }))

  return { paths, fallback: true }
}

export default CaracterInfo
