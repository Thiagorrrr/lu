import MarvelTemplate from '../src/components/templates/Marvel'
import GlobalStyles from '../styles/globals'
import Head from 'next/head'
import Container from '../src/components/atoms/Container'
import Footer from '../src/components/organisms/Footer'

const Marvel = () => (
  <>
    <Head>
      <title>Marvel Personagens</title>
      <meta
        httpEquiv="Content-Security-Policy"
        content="upgrade-insecure-requests"
      />
      <meta name="description" content="Página com os personagens da Marvel" />
      <link rel="icon" href="/favicon.ico" />
      <link rel="preconnect" href="https://fonts.googleapis.com" />
      <link
        rel="preconnect"
        href="https://fonts.gstatic.com"
        crossOrigin="true"
      />
      <link
        href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;500;600;700&display=swap"
        rel="stylesheet"
      />
    </Head>
    <GlobalStyles />
    <Container>
      <MarvelTemplate />
    </Container>
    <Footer />
  </>
)

export default Marvel
