import { createGlobalStyle } from 'styled-components'
import { generateMedia } from 'styled-media-query'
import getVar from '../src/utils/getVar'

export const device = generateMedia({
  mobile: '59.9rem',
  tabletPortrait: '60rem',
  tabletLandscape: '90rem',
  desktop: '120rem',
  desktopLarge: '180rem'
})

const GlobalStyles = createGlobalStyle`
  :root {
    /* ---------- apenas design tokens ---------- */

    /* ---- opacity ---- */
    --opacity-none: 0;
    --opacity-light: 0.2;
    --opacity-medium: 0.4;
    --opacity-intense: 0.6;
    --opacity-semi-opaque: 0.8;

    /* ---- color ---- */
    /* brand colors */
    --color-brand-1-pure: #ff1510;
    --color-brand-2-pure: #fdecec;
    --color-brand-3-pure: #e7f6e7;

    /* typography colors */
    --color-typography-1-pure: #404040;
    --color-typography-2-pure: #8c8c8c;
    --color-typography-2-light: #b9b9b9;


    /* ---- typography ---- */
    /* family */
    --font-family-primary:  'Work Sans', sans-serif;

    /* weights */
    --font-weight-regular: 400;
    --font-weight-medium: 500;
    --font-weight-semi-bold: 600;
    --font-weight-bold: 700;

    /* sizes */
    --font-size-xxus: 1rem;
    --font-size-xus: 1.2rem;
    --font-size-us: 1.4rem;
    --font-size-xxs: 1.6rem;
    --font-size-xs: 1.8rem;
    --font-size-sm: 2rem;
    --font-size-md: 2.4rem;
    --font-size-lg: 3.2rem;
    --font-size-xl: 4.8rem;
    --font-size-xxl: 6.4rem;
    --font-size-xxxl: 7.2rem;
    --font-size-ul: 9.6rem;

    /* line height */
    --line-height-xxus: 1.4rem;
    --line-height-xus: 1.6rem;
    --line-height-us: 1.8rem;
    --line-height-xxs: 2rem;
    --line-height-xs: 2.2rem;
    --line-height-sm: 2.4rem;
    --line-height-md: 3.2rem;
    --line-height-lg: 4.2rem;
    --line-height-xl: 6.4rem;
    --line-height-xxl: 8rem;
    --line-height-xxxl: 8.8rem;
    --line-height-ul: 12rem;

    /* ---- spacing ---- */
    /* stack (margem vertical) */
    --spacing-stack-xxs: 0.4rem;
    --spacing-stack-xs: 0.8rem;
    --spacing-stack-sm: 1.6rem;
    --spacing-stack-md: 2.4rem;
    --spacing-stack-lg: 3.2rem;
    --spacing-stack-xl: 4rem;
    --spacing-stack-xxl: 4.8rem;
    --spacing-stack-xxxl: 5.6rem;
    --spacing-stack-ul: 6.4rem;
    --spacing-stack-xul: 7.2rem;
    --spacing-stack-xxul: 8rem;

    /* inline (margem horizontal) */
    --spacing-inline-xxs: 0.4rem;
    --spacing-inline-xs: 0.8rem;
    --spacing-inline-sm: 1.6rem;
    --spacing-inline-md: 2.4rem;
    --spacing-inline-lg: 3.2rem;
    --spacing-inline-xl: 4rem;
    --spacing-inline-xxl: 4.8rem;
    --spacing-inline-xxxl: 5.6rem;
    --spacing-inline-ul: 6.4rem;
    --spacing-inline-xul: 7.2rem;
    --spacing-inline-xxul: 8rem;

    /* inset (padding -- igual de todos os lados) */
    --spacing-inset-xs: 0.4rem;
    --spacing-inset-sm: 0.8rem;
    --spacing-inset-md: 1.6rem;
    --spacing-inset-lg: 2.4rem;
    --spacing-inset-xl: 3.2rem;
    --spacing-inset-xxl: 4.8rem;

    /* squish (padding -- diferentes na vertical e horizontal) */
    --spacing-squish-xs: 0.4rem 0.8rem;
    --spacing-squish-sm: 0.8rem 1.6rem;
    --spacing-squish-md: 1.6rem 2.4rem;
    --spacing-squish-lg: 1.6rem 3.2rem;

    /* ---- border ---- */
    /* width */
    --border-width-default: 0rem;
    --border-width-sm: 0.1rem;
    --border-width-md: 0.2rem;
    --border-width-lg: 0.4rem;

    /* radius */
    --border-radius-sharp: 0rem;
    --border-radius-pill: 50rem;
    --border-radius-sm: 0.4rem;
    --border-radius-md: 0.8rem;
    --border-radius-lg: 2.4rem;

    /* ---- shadow ---- */
    --shadow-sm: 0 0.1rem 0.4rem rgba(0, 0, 0, 0.1);
    --shadow-md: 0 -0.1rem 0.4rem rgba(0, 0, 0, 0.1);

    /* ---- inner shadow */
    --inner-shadow-sm: 0 -0.1rem 0.4rem rgba(0, 0, 0, 0.1) inset;

    /* ---- transition ---- */
    /* speed */
    --transition-speed-fast: 0.15s;
    --transition-speed-normal: 0.3s;
    --transition-speed-slow: 0.45s;

    /* easing */
    --transition-easing-default: ease;
    --transition-easing-standard: cubic-bezier(0.4, 0, 0.2, 1);
    --transition-easing-decelerated: cubic-bezier(0, 0, 0.2, 1);
    --transition-easing-accelerated: cubic-bezier(0.4, 0, 1, 1);
    --transition-easing-swift-out: cubic-bezier(0.55, 0, 0.1, 1);
  }

  :root {
    box-sizing: border-box;
    color: var(--color-typography-1-pure);
    font-family: var(--font-family-primary);
    font-weight: var(--font-weight-regular);
    font-display: swap;
  }

  *, *::before, *::after {
    box-sizing: inherit;
  }

  html {
    font-size: 62.5%;
    background-color: ${getVar('color-neutral-1-pure')};
  }

  html, body, #__next {
    min-height: 100%;
    margin: 0;
    padding: 0;
  }

  body, #__next {
    font-size: var(--font-size-us);
  }
`

export default GlobalStyles
