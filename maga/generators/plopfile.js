module.exports = (plop) => {
  plop.setGenerator('component', {
    description: 'Gerar um novo componente',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'Qual o nome do componente?'
      },
      {
        type: 'list',
        name: 'type',
        message: 'Qual o tipo do componente?',
        choices: [
          { name: 'Átomo', value: 'atoms' },
          { name: 'Molécula', value: 'molecules' },
          { name: 'Organismo', value: 'organisms' },
          { name: 'Template', value: 'templates' }
        ],
        default: 0
      }
    ],
    actions: (data) => {
      const actions = [
        {
          type: 'add',
          path: '../src/components/{{type}}/{{pascalCase name}}/index.js',
          templateFile: 'templates/component/index.js.hbs'
        },
        {
          type: 'add',
          path: '../src/components/{{type}}/{{pascalCase name}}/{{pascalCase name}}.jsx',
          templateFile: 'templates/component/Component.jsx.hbs'
        },
        {
          type: 'add',
          path: '../src/components/{{type}}/{{pascalCase name}}/styles.js',
          templateFile: 'templates/component/styles.js.hbs'
        }
      ]

      if (data.type === 'atoms' || data.type === 'molecules') {
        actions.push(
          {
            type: 'add',
            path: '../src/components/{{type}}/{{pascalCase name}}/stories.jsx',
            templateFile: 'templates/component/stories.jsx.hbs'
          },
          {
            type: 'add',
            path: '../src/components/{{type}}/{{pascalCase name}}/test.jsx',
            templateFile: 'templates/component/test.jsx.hbs'
          }
        )
      }

      return actions
    }
  })
}
