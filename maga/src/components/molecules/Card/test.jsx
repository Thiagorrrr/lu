import { render, screen } from '@testing-library/react'

import Card from './Card'

describe('<Card />', () => {
  it('should render the heading', () => {
    const { container } = render(<Card />)

    expect(screen.getByRole('heading', { name: /Card/i })).toBeInTheDocument()

    expect(container.firstChild).toMatchSnapshot()
  })
})
