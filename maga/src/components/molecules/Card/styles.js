import styled from 'styled-components'
import { device } from '../../../../styles/globals'

import getVar from '../../../utils/getVar'

export const Wrapper = styled.div`
  ${device.greaterThan('tabletPortrait')`
    max-width: 25%;
    padding-right: ${getVar('spacing-stack-lg')};
  `}
  margin-top: ${getVar('spacing-stack-lg')};

  &:nth-child(4) {
    padding-right: 0;
  }
`
export const Link = styled.a`
  text-decoration: none;
  color: ${getVar('color-typography-1-pure')};
`
export const Title = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-weight: ${getVar('font-weight-bold')};
`
export const List = styled.div`
  list-style: none;
  padding: 0;
`
export const WrapperImg = styled.div`
  max-height: 40rem;
  background-color: ${getVar('color-brand-1-pure')};
`
export const BoxIcon = styled.div`
  cursor: pointer;
`
