import * as S from './styles'
import Image from 'next/image'
import Link from 'next/link'
import Icon from '../../atoms/Icon'

import Text from '../../atoms/Text'
import { useCallback, useState } from 'react'

const Card = ({ item, addFavorite, setFavorite }) => {
  const [select, setSelect] = useState(
    setFavorite?.map((valor) => valor === item.id)[0]
  )

  const favorite = useCallback(() => {
    const isSelected = () => {
      setSelect((select) => !select)
    }
    isSelected()
    addFavorite(item.id, !select)
  }, [addFavorite, item, select])

  return (
    <S.Wrapper>
      <S.List>
        <Link href={`/marvel/${item.id}`} passHref>
          <S.Link title="caracters">
            <S.WrapperImg>
              <Image
                src={`${item?.thumbnail?.path}.${item?.thumbnail?.extension}`}
                height={320}
                width={320}
              ></Image>
            </S.WrapperImg>
          </S.Link>
        </Link>
        <S.Title>
          <Text as="h2" variant="titleCard" marginTop="spacing-inline-sm">
            {item?.name}
          </Text>
          <S.BoxIcon onClick={favorite}>
            {select ? (
              <Icon name="fullHeart" size={20}></Icon>
            ) : (
              <Icon name="emptyHeart" size={22}></Icon>
            )}
          </S.BoxIcon>
        </S.Title>
      </S.List>
    </S.Wrapper>
  )
}

export default Card
