import Card from './Card'

export default {
  title: 'molecules/Card',
  component: Card
}

export const Default = () => <Card />
