import styled, { css } from 'styled-components'

import getVar from '../../../utils/getVar'

const SizeVariations = {
  variant: {
    medium: () => css`
      width: 6rem;
      height: 3.2rem;
    `,

    small: () => css`
      width: 6rem;
      height: 2rem;
    `
  },
  icon: {
    medium: () => css`
      width: 2rem;
      height: 2rem;
    `,

    small: () => css`
      width: 1.5rem;
      height: 1.5rem;
    `
  },
  checked: {
    active: () => css`
      transition: all ${getVar('transition-speed-normal')};
      transform: translate(170%, 0);
    `,
    off: () => css`
      transition: all ${getVar('transition-speed-normal')};
      transform: translate(30%, 0);
    `
  }
}

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  position: relative;
`
export const Input = styled.input`
  height: 100%;
  left: 0;
  margin: 0;
  opacity: 0;
  cursor: pointer;
  position: absolute;
  top: 0;
  width: 100%;
`

export const Box = styled.label`
  ${({ variant }) => css`
    ${!!variant && SizeVariations.variant[variant]}
  `}

  cursor: pointer;
  display: flex;
  background-color: ${getVar('color-typography-2-light')};
  align-items: center;
  border-radius: ${getVar('border-radius-pill')};
`
export const Icon = styled.span`
  ${({ variant, checked }) => css`
    ${checked ? SizeVariations.checked.active : SizeVariations.checked.off}
    ${!!variant && SizeVariations.icon[variant]}
  `}

  display: block;
  content: '';
  border-radius: 20rem;
  background-color: ${getVar('color-brand-1-pure')};
  box-shadow: 0 0.2rem 0.1rem rgba(0, 0, 0, 30%);
`
