import { useState } from 'react'
import * as S from './styles'

const Toggle = ({ id, name, variant = 'medium' }) => {
  const [checked, setChecked] = useState(false)

  const onClick = () => {
    setChecked(checked ? false : true)
  }
  return (
    <S.Wrapper>
      <S.Input
        type="checkbox"
        id={id}
        name={name}
        defaultChecked={checked}
        onClick={onClick}
      />
      <S.Box htmlFor={id} variant={variant}>
        <S.Icon variant={variant} checked={checked}></S.Icon>
      </S.Box>
    </S.Wrapper>
  )
}

export default Toggle
