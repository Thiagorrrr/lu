import Toggle from './Toggle'

export default {
  title: 'atoms/Toggle',
  component: Toggle
}

export const Default = () => <Toggle />
