import styled from 'styled-components'

import { device } from '../../../../styles/globals'

import getVar from '../../../utils/getVar'

export const Wrapper = styled.div`
  max-width: 160rem;
  margin-right: auto;
  margin-left: auto;
  padding-right: ${getVar('spacing-inline-md')};
  padding-left: ${getVar('spacing-inline-md')};

  ${device.greaterThan('tabletLandscape')`
    padding-right: ${getVar('spacing-inline-xxxl')};
    padding-left: ${getVar('spacing-inline-xxxl')};
  `}
`
