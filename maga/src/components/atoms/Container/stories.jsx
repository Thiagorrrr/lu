import Container from './Container'

export default {
  title: 'atoms/Container',
  component: Container
}

export const Default = () => <Container />
