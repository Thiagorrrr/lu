import styled, { css } from 'styled-components'

import getVar from '../../../utils/getVar'

export const Wrapper = styled.span`
  ${({ size }) => css`
    display: inline-block;
    ${!!size &&
    css`
      width: ${size / 10}rem;
      height: ${size / 10}rem;
    `}

    svg * {
      transition: all ${getVar('transition-speed-normal')}
        ${getVar('transition-easing-standard')};
    }
  `}
`
