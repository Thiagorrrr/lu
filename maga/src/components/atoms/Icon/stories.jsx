import Icon from './Icon'

export default {
  title: 'atoms/Icon',
  component: Icon
}

export const Default = () => <Icon />
