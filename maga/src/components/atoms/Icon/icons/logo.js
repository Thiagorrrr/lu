import getVar from '../../../../utils/getVar'

const Chat = ({ color = 'color-brand-1-pure', size = 32 }) => (
  <svg
    width={size}
    height={size}
    viewBox="0 0 32 32"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M24 14.217c0 2.87-2.348 5.218-5.217 5.218h-2.087l-2.783 2.782v-2.782h-.696c-2.87 0-5.217-2.348-5.217-5.218S10.348 9 13.217 9h5.566C21.653 9 24 11.348 24 14.217z"
      stroke={getVar(color)}
      strokeMiterlimit="10"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export default Chat
