import dynamic from 'next/dynamic'
import * as S from './styles'

const Icon = ({ name, size = 32 }) => {
  const IconSVG = dynamic(() => import(`./icons/${name}`))

  return (
    <S.Wrapper size={size}>
      <IconSVG size={size} />
    </S.Wrapper>
  )
}

export default Icon
