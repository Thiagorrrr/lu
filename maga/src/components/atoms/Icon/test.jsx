import { render, screen } from '@testing-library/react'

import Icon from './Icon'

describe('<Icon />', () => {
  it('should render the heading', () => {
    const { container } = render(<Icon />)

    expect(screen.getByRole('heading', { name: /Icon/i })).toBeInTheDocument()

    expect(container.firstChild).toMatchSnapshot()
  })
})
