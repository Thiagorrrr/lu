import styled, { css } from 'styled-components'

import getVar from '../../../utils/getVar'

const TextVariations = {
  color: {
    'brand-1-pure': () =>
      css`
        color: ${getVar('color-brand-1-pure')};
      `,

    'brand-2-pure': () =>
      css`
        color: ${getVar('color-brand-2-pure')};
      `,

    'brand-3-pure': () =>
      css`
        color: ${getVar('color-brand-3-pure')};
      `,

    'typography-1-pure': () =>
      css`
        color: ${getVar('color-typography-1-pure')};
      `,

    'typography-2-pure': () =>
      css`
        color: ${getVar('color-typography-2-pure')};
      `,

    'typography-1-light': () =>
      css`
        color: ${getVar('color-typography-1-light')};
      `
  },
  variant: {
    'headline-large': () => css`
      font-size: ${getVar('font-size-xl')};
      line-height: ${getVar('line-height-xl')};
    `,

    'headline-medium': () => css`
      font-size: ${getVar('font-size-lg')};
      line-height: ${getVar('line-height-lg')};
    `,

    'headline-small': () => css`
      font-size: ${getVar('font-size-md')};
      line-height: ${getVar('line-height-md')};
    `,

    subtitle: () => css`
      font-size: ${getVar('font-size-xxs')};
      line-height: ${getVar('line-height-xxs')};
    `,

    body: () => css`
      font-size: ${getVar('font-size-us')};
      line-height: ${getVar('line-height-us')};
    `,

    'body-small': () => css`
      font-size: ${getVar('font-size-xus')};
      line-height: ${getVar('line-height-xus')};
    `,

    caption: () => css`
      font-size: ${getVar('font-size-xxus')};
      line-height: ${getVar('line-height-xxus')};
    `,

    principal: () => css`
      font-size: ${getVar('font-size-xl')};
      line-height: ${getVar('line-height-xl')};
      font-weight: ${getVar('font-weight-bold')};
      text-transform: uppercase;
    `,

    subtitleCard: () => css`
      font-size: ${getVar('font-size-us')};
      line-height: ${getVar('line-height-us')};
      font-weight: ${getVar('font-weight-bold')};
    `,

    titleCard: () => css`
      font-size: ${getVar('font-size-xxs')};
      line-height: ${getVar('line-height-xxs')};
      font-weight: ${getVar('font-weight-bold')};
      text-transform: uppercase;
      text-decoration: none;
    `
  }
}

export const Text = styled.span`
  ${({ variant, color, marginTop, marginBottom }) => css`
    margin-top: ${marginTop
      ? marginTop === 'none'
        ? 0
        : getVar(marginTop)
      : null};
    margin-bottom: ${marginBottom
      ? marginBottom === 'none'
        ? 0
        : getVar(marginBottom)
      : null};
    font-family: ${getVar('font-family-primary')};
    font-weight: ${getVar('font-weight-regular')};

    ${!!color && TextVariations.color[color]}
    ${!!variant && TextVariations.variant[variant]}
  `}

  a {
    color: ${getVar('color-brand-1-pure')};
    text-underline-offset: ${getVar('spacing-inset-xs')};
    border: 0;
    background: none;

    &:not(:disabled) {
      &:hover {
        color: ${getVar('color-typography-2-pure')};
        text-decoration: underline;
      }

      &:active {
        color: ${getVar('color-typography-2-pure')};
      }
    }

    &:disabled {
      color: ${getVar('color-typography-2-light')};
    }
  }

  strong,
  b {
    font-weight: ${getVar('font-weight-medium')};
  }
`
