import * as S from './styles'

const Text = ({
  variant = 'body',
  marginTop,
  marginBottom,
  as,
  children,
  color
}) => {
  return (
    <S.Text
      variant={variant}
      marginTop={marginTop}
      marginBottom={marginBottom}
      as={as}
      color={color}
    >
      {children}
    </S.Text>
  )
}

export default Text
