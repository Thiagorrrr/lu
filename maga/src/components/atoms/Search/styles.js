import styled from 'styled-components'
import { device } from '../../../../styles/globals'
import getVar from '../../../utils/getVar'

export const Wrapper = styled.div`
  margin: ${getVar('spacing-inline-lg')};
  width: 100%;
  ${device.greaterThan('tabletPortrait')`
    width: 60%;
  `}
`
export const Label = styled.label`
  display: flex;
  align-items: center;
  background-color: ${getVar('color-brand-2-pure')};
  border-radius: ${getVar('border-radius-pill')};
  padding: 2rem;
`
export const InputValue = styled.input`
  display: block;
  height: 100%;
  width: 100%;
  border: 0;
  font-family: ${getVar('font-family-primary')};
  font-weight: ${getVar('font-weight-regula')};
  font-size: ${getVar('font-size-xs')};
  color: ${getVar('color-brand-1-pure')};
  margin-left: ${getVar('spacing-inline-md')};
  background-color: ${getVar('color-brand-2-pure')};
  outline: transparent;

  &::-webkit-input-placeholder {
    color: ${getVar('color-brand-1-pure')};
  }

  &:-moz-placeholder {
    color: ${getVar('color-brand-1-pure')};
  }

  &::-moz-placeholder {
    color: ${getVar('color-brand-1-pure')};
  }

  &:-ms-input-placeholder {
    color: ${getVar('color-brand-1-pure')};
  }

  &:-webkit-autofill,
  &:-webkit-autofill:hover,
  &:-webkit-autofill:focus {
    box-shadow: 0 0 0 1000rem ${getVar('color-brand-2-pure')} inset;
  }

  &::-webkit-inner-spin-button,
  &::-webkit-calendar-picker-indicator {
    display: none;
    -webkit-appearance: none;
  }
`
