import * as S from './styles'
import Icon from '../Icon'

const Search = ({ caracterByName }) => {
  return (
    <S.Wrapper>
      <form onSubmit={caracterByName}>
        <div>
          <S.Label htmlFor="search">
            <Icon name="lupa" size="19" />
            <S.InputValue
              name="search"
              id="search"
              type="text"
              placeholder="Procure por hérois"
            />
          </S.Label>
        </div>
      </form>
    </S.Wrapper>
  )
}

export default Search
