import Search from './Search'

export default {
  title: 'atoms/Search',
  component: Search
}

export const Default = () => <Search />
