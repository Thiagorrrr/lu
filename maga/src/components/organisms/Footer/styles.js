import styled from 'styled-components'

import getVar from '../../../utils/getVar'

export const Wrapper = styled.div``
export const FooterWrapper = styled.div`
  background-color: ${getVar('color-brand-1-pure')};
  height: 8rem;
  margin-top: ${getVar('spacing-inline-xxl')};
`
