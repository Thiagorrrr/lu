import * as S from './styles'

const Footer = () => (
  <S.Wrapper>
    <S.FooterWrapper></S.FooterWrapper>
  </S.Wrapper>
)

export default Footer
