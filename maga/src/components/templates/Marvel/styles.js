import styled from 'styled-components'
import { device } from '../../../../styles/globals'
import getVar from '../../../utils/getVar'

export const Wrapper = styled.div``

export const WrapperToggle = styled.div`
  margin: 0 ${getVar('spacing-inline-lg')};
`
export const Box = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-wrap: wrap;
  margin: ${getVar('spacing-inline-sm')} 0;
`
export const BoxOrder = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  flex-direction: column;
  width: 100%;
  ${device.greaterThan('tabletPortrait')`
    flex-direction: row;
    flex-wrap: initial;
    width: auto;
  `}
`
export const BoxHero = styled.div`
  display: flex;
  width: 100%;
  padding-top: ${getVar('spacing-inline-sm')};
  padding-bottom: ${getVar('spacing-inline-sm')};
  ${device.greaterThan('tabletLandscape')`
    flex-wrap: wrap;
    flex-direction: row;
    align-items: center;
    width: auto;
  `}
`
export const List = styled.div`
  padding: 0;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  ${device.greaterThan('tabletLandscape')`
    justify-content: space-between;
  `}
`
export const SubText = styled.div`
  text-align: center;
  color: ${getVar('color-typography-2-pure')};
  ${device.greaterThan('tabletPortrait')`
    text-align: left;
  `}
`
export const WrapperImage = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`
export const WrapperFavorite = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
`
