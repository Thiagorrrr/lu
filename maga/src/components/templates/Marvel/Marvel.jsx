import * as S from './styles'
import Image from 'next/image'

import Text from '../../../components/atoms/Text'
import Card from '../../../components/molecules/Card'
import Search from '../../../components/atoms/Search'
import Toggle from '../../../components/atoms/Toggle'
import Icon from '../../../components/atoms/Icon'

import {
  getCaracterByName,
  getOrderByName,
  useCaracterData
} from '../../../connection/functions'
import { useEffect, useState } from 'react'

const Marvel = () => {
  const [todos, setTodos] = useState([])
  const [stored, setStored] = useState()
  const [byname, setByname] = useState(true)
  const { data } = useCaracterData()
  const [dataCaracters, setDataCaracters] = useState()

  useEffect(() => {
    const stor = JSON.parse(sessionStorage?.getItem('favoritos'))
    setStored(stor)
  }, [setStored])

  const orderByFavorite = () => {
    // setFavorite((favorite) => !favorite)
  }

  const orderByName = async () => {
    const value = await getOrderByName(byname)
    if (!value) return
    setDataCaracters(value)
    setByname(byname ? false : true)
  }

  const caracterByName = async (name) => {
    if (name) name.preventDefault()
    const resultName = await getCaracterByName(name.target.search.value)
    if (!resultName) return
    setDataCaracters(resultName)
  }

  const addFavorite = (value, select) => {
    if (todos?.length > 4) return

    if (select) {
      setTodos([...todos, value])
      sessionStorage.setItem('favoritos', JSON.stringify([...todos, value]))
    } else {
      sessionStorage?.setItem(
        'favoritos',
        JSON.stringify(
          stored?.filter((item) => item !== value) ||
            JSON.stringify(todos?.filter((item) => item !== value))
        )
      )
      setTodos(
        todos?.filter((item) => item !== value) ||
          stored?.filter((item) => item !== value)
      )
    }
  }

  return (
    <S.Wrapper>
      <S.WrapperImage>
        <Image src={'/static/images/logo.png'} width={300} height={125}></Image>
        <Text as="h1" variant="principal">
          Explore o universo
        </Text>
        <S.SubText>
          <Text variant="subtitle" color="typography-2-pure">
            Mergulhe no domínio deslumbrante de todos os personagens clássicos
            que você ama - e aqueles que você descobrirá em breve!
          </Text>
        </S.SubText>
        <Search caracterByName={caracterByName} />
      </S.WrapperImage>
      <S.Box>
        <Text variant="subtitle" color="typography-2-pure">
          {dataCaracters
            ? dataCaracters?.data?.results &&
              dataCaracters?.data?.results?.length > 1
              ? `Encontrados ${dataCaracters?.data?.results?.length} heróis`
              : `Encontrado ${dataCaracters?.data?.results?.length} herói`
            : data?.data?.results && data?.data?.results?.length > 1
            ? `Encontrados ${data?.data?.results?.length} heróis`
            : `Encontrado ${data?.data?.results?.length} herói`}
        </Text>
        <S.BoxOrder>
          <S.BoxHero>
            <Icon name="hero" size={28}></Icon>
            <Text variant="subtitle" color="brand-1-pure">
              Ordernar por nome - A/Z
            </Text>
            <S.WrapperToggle onClick={orderByName}>
              <Toggle />
            </S.WrapperToggle>
          </S.BoxHero>
          <S.WrapperFavorite onClick={orderByFavorite}>
            <Icon name="heart" size={35}></Icon>
            <Text variant="subtitle" color="brand-1-pure">
              Somente favoritos
            </Text>
          </S.WrapperFavorite>
        </S.BoxOrder>
      </S.Box>
      <S.List>
        {dataCaracters
          ? dataCaracters?.data?.results?.map((item) => (
              <Card
                setFavorite={stored?.filter((valor) => valor === item.id)}
                addFavorite={addFavorite}
                key={item.id}
                item={item}
              />
            ))
          : data?.data?.results?.map((item) => (
              <Card
                setFavorite={stored?.filter((valor) => valor === item.id)}
                addFavorite={addFavorite}
                key={item.id}
                item={item}
              />
            ))}
      </S.List>
    </S.Wrapper>
  )
}

export default Marvel
