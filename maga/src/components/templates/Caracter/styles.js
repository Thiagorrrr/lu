import styled from 'styled-components'
import { device } from '../../../../styles/globals'
import getVar from '../../../utils/getVar'

export const Wrapper = styled.div``

export const ContainerIconText = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: ${getVar('spacing-stack-sm')};
  ${device.greaterThan('tabletPortrait')`
    justify-content: center;
  `}
`
export const WrapperIconText = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: center;
  margin-top: ${getVar('spacing-stack-xs')};
`
export const BoxIconText = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-right: ${getVar('spacing-stack-xl')};
`
export const BoxImageText = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  margin-bottom: ${getVar('spacing-stack-xul')};
  ${device.greaterThan('tabletPortrait')`
    flex-direction: row;
  `}
`
export const BoxLast = styled.div`
  display: flex;
  margin-top: ${getVar('spacing-stack-md')};
`
export const WrapperText = styled.div`
  display: block;
  max-width: 100%;
  margin-bottom: ${getVar('spacing-stack-md')};
  ${device.greaterThan('tabletPortrait')`
    max-width: 30%;
    margin-bottom: 0;
  `}
`
export const List = styled.ul`
  list-style: none;
  padding: 0;
  display: flex;
  flex-wrap: wrap;
  ${device.greaterThan('tabletPortrait')`
    max-width: 70%;
  `}
`
export const ListItem = styled.li`
  display: flex;
  padding: 1.6rem;
  max-width: 15rem;
  flex-direction: column;
  align-items: center;
`
export const Spacing = styled.div`
  margin-right: ${getVar('spacing-stack-xs')};
`
