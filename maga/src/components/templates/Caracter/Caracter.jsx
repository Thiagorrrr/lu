import * as S from './styles'
import moment from 'moment'
import 'moment/locale/pt-br'

import Text from '../../atoms/Text'
import Image from 'next/image'
import Footer from '../../organisms/Footer'
import Icon from '../../atoms/Icon'

import Container from '../../atoms/Container'

import { ColorExtractor } from 'react-color-extractor'

import { getCaracterComics } from '../../../connection/functions'

import { useEffect, useState } from 'react'

const Caracter = ({ data }) => {
  const [color, setColor] = useState()
  const [comics, setComics] = useState()
  useEffect(() => {
    const getComic = async () => {
      const comic = await getCaracterComics(data?.data?.results[0]?.id)
      return setComics(comic)
    }
    getComic()
  }, [data])

  const getColors = (colors) => setColor([...colors, colors])
  const hex2rgba = (hex, alpha = 1) => {
    const [r, g, b] = hex.match(/\w\w/g).map((x) => parseInt(x, 16))
    return `rgba(${r},${g},${b},${alpha})`
  }
  return (
    <div
      style={{
        backgroundColor: color && hex2rgba(color[5], 0.3)
      }}
    >
      <Container>
        <S.BoxImageText>
          <S.WrapperText>
            <Text as="h1" variant={'principal'}>
              {data?.data?.results[0]?.name}
            </Text>
            <Text color="typography-1-pure" as="p">
              {data?.data?.results[0]?.description}
            </Text>

            <S.ContainerIconText>
              <S.BoxIconText>
                <Text
                  variant={'subtitleCard'}
                  color="typography-1-pure"
                  marginBottom={'spacing-inline-xs'}
                >
                  Quadrinhos
                </Text>
                <S.WrapperIconText>
                  <Icon name="book" size={35}></Icon>
                  <Text variant={'subtitleCard'} color="typography-1-pure">
                    {data?.data?.results[0]?.comics?.available}
                  </Text>
                </S.WrapperIconText>
              </S.BoxIconText>
              <S.BoxIconText>
                <Text
                  variant={'subtitleCard'}
                  color="typography-1-pure"
                  marginBottom={'spacing-inline-xs'}
                >
                  Filmes
                </Text>
                <S.WrapperIconText>
                  <Icon name="video" size={35}></Icon>
                  <Text variant={'subtitleCard'} color="typography-1-pure">
                    {data?.data?.results[0]?.series?.available}
                  </Text>
                </S.WrapperIconText>
              </S.BoxIconText>
            </S.ContainerIconText>
            <S.BoxLast>
              <S.Spacing>
                <Text variant={'subtitleCard'} color="typography-1-pure">
                  Ultimo quadrinho:
                </Text>
              </S.Spacing>
              <Text variant={'subtitleCard'} color="typography-1-pure">
                {moment(comics?.data?.results[0]?.dates[0]?.date).format('ll')}
              </Text>
            </S.BoxLast>
          </S.WrapperText>
          <ColorExtractor getColors={getColors}>
            <img
              src={`${data?.data?.results[0]?.thumbnail?.path}.${data?.data?.results[0]?.thumbnail?.extension}`}
              style={{ width: 0, height: 0 }}
            />
          </ColorExtractor>
          <Image
            src={`${data?.data?.results[0]?.thumbnail?.path}.${data?.data?.results[0]?.thumbnail?.extension}`}
            height={500}
            width={600}
          ></Image>
        </S.BoxImageText>

        <Text variant={'titleCard'}>Ultimos lançamentos </Text>
        <S.List>
          {comics?.data?.results.map((item, index) => (
            <>
              {index <= 10 && (
                <S.ListItem key={item.id}>
                  {item?.images[0] && (
                    <Image
                      src={`${item?.images[0].path}.${item?.images[0].extension}`}
                      width={150}
                      height={200}
                    ></Image>
                  )}
                  <Text variant="subtitle" marginTop="spacing-inline-xs">
                    {item.title}
                  </Text>
                </S.ListItem>
              )}
            </>
          ))}
        </S.List>
      </Container>
      <Footer />
    </div>
  )
}

export default Caracter
