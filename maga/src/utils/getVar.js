/**
 * Imprime a custom propery (variável css) de acordo
 * com as que temos listadas no projeto.
 * @param {VarNames} varName
 * @returns var(--varName)
 * @example
 * `color: ${getVar('color-brand-1-pure')};`
 * // color: var(--color-brand-1-pure);
 */
function getVar(varName) {
  return `var(--${varName})`
}

export default getVar
