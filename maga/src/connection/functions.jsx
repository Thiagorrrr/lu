import useSWR from 'swr'
import { PUBLIC_KEY, HASH_KEY } from '../utils/constants'

export function useCaracterData() {
  const url = `http://gateway.marvel.com/v1/public/characters?ts=1&limit=20&apikey=${PUBLIC_KEY}&hash=${HASH_KEY}`

  const { data, error, mutate } = useSWR(url, async () => {
    const response = await fetch(url)
    const data = await response.json()

    if (error) return
    if (!data) return
    return data
  })

  return { data, error, mutate }
}

export const getOrderByName = async (order) => {
  try {
    const url = `http://gateway.marvel.com/v1/public/characters?ts=1&limit=20&${`${order? `orderBy=-name`: ''}`}&apikey=${PUBLIC_KEY}&hash=${HASH_KEY}`
    const response = await fetch(url)

    if (!response.ok) throw Error

    const data = await response.json()

    return data
  } catch (error) {
    console.error(error)
    return null
  }
}

export const getCaracterByName = async (name) => {
  try {
    const url = `http://gateway.marvel.com/v1/public/characters?ts=1&limit=20&${`${name? `name=${name}`: ''}`}&orderBy=name&apikey=${PUBLIC_KEY}&hash=${HASH_KEY}`

    console.log(url,'sddsds');
    const response = await fetch(url)

    if (!response.ok) throw Error

    const data = await response.json()

    return data
  } catch (error) {
    console.error(error)
    return null
  }
}

export const getCaracterComics = async (id) => {
  try {

    const url = `http://gateway.marvel.com/v1/public/characters/${id}/comics?ts=1&apikey=${PUBLIC_KEY}&hash=${HASH_KEY}`


    const response = await fetch(url)

    if (!response.ok) throw Error

    const comic = await response.json()

    return comic
  } catch (error) {
    console.error(error)
    return null
  }
}
